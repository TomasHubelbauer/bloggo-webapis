# Web APIs

- [ ] Check out [Javascript APIs Current Status](https://www.w3.org/standards/techs/js#w3c_all)
- [ ] Check out [JavaScript APIs](https://developer.chrome.com/apps/api_index)

- [ ] Enumerate Web APIs from mainstream browsers (either through docs scraping or reflection on `window` in the browsers themselves)
- [ ] Use Puppeteer to automatically walk `window` in Chromium and produce a MarkDown with API links and browser version info
- [ ] Use Selenium to automatically walk `window` in Firefox and produce a MarkDown with API links and browser version info
- [ ] Create an dict of `path` to notes so that notes for each path can be attached
- [ ] Add a away to show which paths are deprecated, non-standard, renderer-specific etc.
